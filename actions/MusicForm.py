class MusicForm(FormAction):
   """Example of a custom form action"""

   def name(self):
       # type: () -> Text
       """Unique identifier of the form"""

       return "music_form"

   @staticmethod
   def required_slots(tracker):
       # type: () -> List[Text]
       """A list of required slots that the form has to fill"""

       return ["music_name"]

   def submit(self, dispatcher, tracker, domain):
       # what your action should do
        music = tracker.get_slot("music_name")
        response = get_music(music)
        dispatcher.utter_message(response)  # send the message back to the user
        return [SlotSet("music_name", None)]
        
        
        
        
        
        
        
        
def get_music(music):
    res = "Playing {} .....".format(music)
    return res        