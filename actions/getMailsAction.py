
from typing import Dict, Text, Any, List, Union, Optional
from rasa_core_sdk import Action
from rasa_core_sdk import ActionExecutionRejection
from rasa_core_sdk import Tracker


logger = logging.getLogger(__name__)

class ActionNewMail(Action):
    def name(self):
        # define the name of the action which can then be included in training stories
        return "action_getMails"

    def run(self, dispatcher, tracker, domain):
        response = get_mails()
        dispatcher.utter_message(response)  # send the message back to the user
        return []
        
def get_mails():
    string = "getting new mails in your inbox"
    return string