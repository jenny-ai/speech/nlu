class FoodForm(FormAction):
   """Example of a custom form action"""

   def name(self):
       # type: () -> Text
       """Unique identifier of the form"""

       return "food_form"

   @staticmethod
   def required_slots(tracker):
       # type: () -> List[Text]
       """A list of required slots that the form has to fill"""

       return ["food"]

   def submit(self, dispatcher, tracker, domain):
       # what your action should do
        food = tracker.get_slot("food")
        response = get_food(food)
        dispatcher.utter_message(response)  # send the message back to the user
        return [SlotSet("food", None)]
        
        
        
        
        
        
        
        
        
        
def get_food(food):
    res = "I have sent you a list of {} resturants on your phone".format(food)
    return res
        