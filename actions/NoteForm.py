

from typing import Dict, Text, Any, List, Union, Optional
from rasa_core_sdk import Action
from rasa_core_sdk import ActionExecutionRejection
from rasa_core_sdk import Tracker
from rasa_core_sdk.events import SlotSet
from rasa_core_sdk.executor import CollectingDispatcher
from rasa_core_sdk.forms import FormAction, REQUESTED_SLOT


class NoteForm(FormAction):
    def name(self):
        # type: () -> Text
        return "note_form"

    @staticmethod
    def required_slots(tracker: Tracker) -> List[Text]:
        """A list of required slots that the form has to fill"""
        return ["note"]

    def submit(self,
                dispatcher: CollectingDispatcher,
                tracker: Tracker,
                domain: Dict[Text, Any]) -> List[Dict]:
        """Define what the form has to do
            after all required slots are filled"""
        # utter submit template
        note = tracker.get_slot('note')
        response = set_note(note)
        dispatcher.utter_message(response)  
        return [SlotSet("note", None)]


def set_note(note):
    string = "set [{}] as a note".format(note)
    return string