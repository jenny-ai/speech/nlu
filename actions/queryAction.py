from rasa_core_sdk import Action
from rasa_core_sdk import ActionExecutionRejection
from rasa_core_sdk import Tracker
from rasa_core_sdk.events import SlotSet


class ActionQuery(Action):
    def name(self):

        return "action_queries"

    def run(self, dispatcher, tracker, domain):

        query = tracker.get_slot("query")
        # in case of place entity
        #if(query==None):
        #    query = tracker.get_slot("place")
        response = get_query(query)
        dispatcher.utter_message(response) 
        return [SlotSet("query", None)]
        
        

def get_query(query):
    string = "searching for {}".format(query)
    return string
