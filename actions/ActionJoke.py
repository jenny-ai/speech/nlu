class ActionJoke(Action):
    def name(self):
        # define the name of the action which can then be included in training stories
        return "action_joke"

    def run(self, dispatcher, tracker, domain):
        # what your action should do
        response = get_joke()
        dispatcher.utter_message(response)  # send the message back to the user
        return []

class ActionNotJoke(Action):
    def name(self):
        # define the name of the action which can then be included in training stories
        return "action_not_joke"

    def run(self, dispatcher, tracker, domain):
        # what your action should do
        response = get_not_joke()
        dispatcher.utter_message(response)  # send the message back to the user
        return []
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
def get_joke():
    res = "Do you want to hear another joke?"
    return res

def get_not_joke():
    res = "Okay"
    return res