class WakeForm(FormAction):
   """Example of a custom form action"""

   def name(self):
       # type: () -> Text
       """Unique identifier of the form"""

       return "wake_form"

   @staticmethod
   def required_slots(tracker):
       # type: () -> List[Text]
       """A list of required slots that the form has to fill"""

       return ["time", "date"]

   def submit(self, dispatcher, tracker, domain):
       # what your action should do
        date = tracker.get_slot("date")
        time = tracker.get_slot("time")
        response = get_wake(date, time)
        dispatcher.utter_message(response)  # send the message back to the user
        return []


class ActionWakeUpAffirm(Action):
    def name(self):
        # define the name of the action which can then be included in training stories
        return "action affirm wake"

    def run(self, dispatcher, tracker, domain):
        # what your action should do
        date = tracker.get_slot("date")
        time = tracker.get_slot("time")
        response = get_affirm_wake(date, time)
        dispatcher.utter_message(response)  # send the message back to the user
        return [SlotSet("time", None),SlotSet("date", None)]

class ActionWakeUpDeny(Action):
    def name(self):
        # define the name of the action which can then be included in training stories
        return "action deny wake"

    def run(self, dispatcher, tracker, domain):
        # what your action should do
        response = get_deny_wake()
        dispatcher.utter_message(response)  # send the message back to the user
        return []
        
        
        
        
        
        
        
        
        
        
        

        
def get_wake(date, time):
    res = "you want me to set you an alarm at {0} {1}, do you confirm that?".format(time, date)
    return res

def get_affirm_wake(date, time):
    res = "i have set you an alarm at {0} {1}".format(time, date)
    return res

def get_deny_wake():
    res = "Please ask again"
    return res