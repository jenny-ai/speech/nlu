 
## story_goodbye
* goodbye
 - utter_goodbye

## story_thanks
* thanks
 - utter_thanks
 
## story_name
* name{"name":"Sam"}
 - utter_greet
 

## story_query_01
* queries{"query":"El Ahly team"}
 - action_queries

## story_query_02
* name{"name":"Ahmed"}
 - utter_greet
* queries{"query":"eiffel tower desginer"}
 - action_queries
* thanks
 - utter_thanks
* goodbye
 - utter_goodbye 

## story_query_03
* queries{"query":"elsherouk city"}
 - action_queries
* thanks
 - utter_thanks


## story_location_01
* provide_location{"place":"eiffel tower desginer"}
 - location_form
 - form{"name":"location_form"}
 - form{"name":"null"}


## story_location_02
* name{"name":"mohammed"}
 - utter_greet
* provide_location
 - location_form
 - form{"name":"location_form"}
 - slot{"requested_slot": "place"}
* form : inform_place{"place":"tahrir square"}
 - location_form
 - slot {"place":"tahrir square"}
 - form{"name":"null"}
* thanks
 - utter_thanks
* goodbye
 - utter_goodbye 

## story_location_03
* provide_location
 - location_form
 - form{"name":"location_form"}
 - slot{"requested_slot": "place"}
* inform_place{"place":"ainshams"}
 - location_form
 - slot {"place":"ainshams"}
 - form{"name":"null"}
* thanks
 - utter_thanks
* goodbye
 - utter_goodbye 

## story_location_04
* provide_location{"place": "abasyia"}
 - location_form
 - form{"name":"location_form"}
 - form{"name":"null"}
* goodbye
 - utter_goodbye


## story_01
* name{"name":"Abbas"}
 - utter_greet
* queries{"query":"programming languages"}
 - action_queries
* provide_location
 - location_form
 - form{"name":"location_form"}
 - slot{"requested_slot": "place"}
* form : inform_place{"place":"soho square"}
 - location_form
 - slot {"place":"soho square"}
 - form{"name":"null"}
* thanks
 - utter_thanks
* goodbye
 - utter_goodbye 

## story_new_mails_01
* get_new_mails
 - action_getMails

## story_new_mails_01
* name{"name":"Ahmed"}
 - utter_greet
* get_new_mails
 - action_getMails
* thanks
 - utter_thanks
* goodbye
 - utter_goodbye 


## story_note
* set_note
 - note_form
 - form{"name":"note_form"}
* inform_note
 - note_form
 - slot {"note":"ahmed borrowed my physics book"}
 - form{"name":"null"}


## story_joke_01
* joke
 - action_joke
* affirm
 - action_joke

## story_joke_02
* joke
 - action_joke
* deny
 - action_not_joke

## story_joke_03
* greet
 - utter_name
* name{"name":"Lucy"}
 - utter_greet
* joke
 - action_joke
* thanks
 - utter_thanks
* goodbye
 - utter_goodbye 

## story_ask_to_wake_up_01
* ask to wake up at a certain time
 - wake_form
 - form{"name": "food_form"}
 - form{"name": null}
* affirm
 - action affirm wake

## story_ask_to_wake_up_02
* ask to wake up at a certain time
 - wake_form
 - form{"name": "food_form"}
 - form{"name": null}
* deny
 - action deny wake

## story_recommendations_for_food_restaurants_01
* recommendations for food restaurants
 - food_form
 - form{"name": "food_form"}
 - form{"name": null}

## story_play_music_01
* play music/ audio book
 - music_form
 - form{"name": "music_form"} 
 - form{"name": null}
