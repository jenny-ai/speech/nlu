## intent:goodbye
- Goodbye
- See you later
- Bye jenny
- bye
- bye for now
- see you later
- See you
- goodnight
- have a nice day
- we'll speak soon

## intent:thanks
- Thanks
- Thank you
- Thank you so much
- Thanks bot
- Thanks for that
- cheers
- cheers bro
- ok thanks!
- perfect thank you
- thanks a bunch for everything
- thanks for the help
- thanks a lot
- amazing, thanks
- cool, thanks
- cool thank you
- ok thanks

## intent:affirm
- yes
- yes sure
- absolutely
- for sure
- yes yes yes
- definitely


## intent:name
- My name is [Juste](name)
- I am [Josh](name)
- I'm [Lucy](name)
- People call me [Greg](name)
- It's [David](name)
- Usually people call me [Amy](name)
- My name is [John](name)
- You can call me [Sam](name)
- Please call me [Linda](name)
- Name name is [Tom](name)
- I am [Richard](name)
- I'm [Tracy](name)
- Call me [Sally](name)
- I am [Philipp](name)
- I am [Charlie](name)
- I am [Charlie](name)
- I am [Ben](name)
- Call me [Susan](name)
- [Lucy](name)
- [Peter](name)
- [Mark](name)
- [Joseph](name)
- [Tan](name)
- [Pete](name)
- [Elon](name)
- [Penny](name)
- name is [Andrew](name)
- I [Lora](name)
- [Stan](name) is my name
- [Susan](name) is the name
- [Ross](name) is my first name
- [Bing](name) is my last name
- Few call me as [Angelina](name)
- Some call me [Julia](name)
- Everyone calls me [Laura](name)
- I am [Ganesh](name)
- My name is [Mike](name)
- just call me [Monika](name)
- Few call [Dan](name)
- You can always call me [Suraj](name)
- Some will call me [Andrew](name)
- My name is [Ajay](name)
- I call [Ding](name)
- I'm [Partia](name)
- Please call me [Leo](name)
- name is [Pari](name)
- name [Sanjay](name)



## intent:queries
- i need information about [English Football League](query)
- what is [the capital of Egypt](query)?
- who [invented the light bulb](query)?
- who is [the richest man in the world](query)?
- what is [the quantum mechanics](query)?
- who is [yanni](query)?
- Search for [the meaning of love](query)
- who is [messi](query) ?
- what is [the differnce between mean and mode](query)
- give me information about [classical mathmatics](query)



## intent:provide_location
- send me the location of [Eiffel tower](place)
- please i want to know the location of [Giza pyramides](place)
- tell me where is [carrefour](place)
- i want to know the location of [cairo tower](place)
- show me the location of [egyptian museum](place)
- where is [karnak temple](place)
- the location of [citadel of Qaitbay](place)
- give me the location of [the hanging church](place)
- tell me the location of [AlAzhar park](place)
- i want to know the location of [mosque of Muhammad Ali](place) on the map
- where is [Statue of Liberty](place) on the map

## intent:inform_place
- [cairo fesitival](place)
- the place is [sun city mall](place)
- the location of [Ainshams university](place)

## intent:get_new_mails
- get my new mails from my account please
- i need to know the new mails which have sent to me
- tell me the new mails 
- any new mail
- get any new mail form my email
- any new mails in my inbox 
- what is new in my mail inbox

## intent:set_note
- please set a note
- i want to set a note
- set my note please 
- i need to set a note
- set a note
- set  note
- save this note 

## intent:inform_note
- [my sister has broken her mobile](note)
- [my doctor need an assignment next week](note)
- [i want to get my clothes from the dry clean](note)
- [I got the full mark in the project](note)
- [i have to buy the vegetables to my mother](note)
- [i paid the internet bill](note)
- [i have an important meeting](note)


## intent:joke
- Can you tell me a joke?
- I would like to hear a joke
- Tell me a joke
- A joke please
- Tell me a joke please
- I would like to hear a joke
- I would loke to hear a joke, please
- Can you tell jokes?
- Please tell me a joke
- I need to hear a joke
- tell me a joke
- joke
- another joke
- give me a joke
- tell me something funny
- make me laugh 
- I want to laugh

## intent:deny
- no
- i can't
- another time
- some other time

## intent:ask to wake up at a certain time
- wake me up [today](date) at [6 AM](time)
- set an alarm [today](date) at [7 PM](time)
- I want to wake up [tomorrow](date) at [7 AM](time)
- can you wake me up [tomorrow](date) at [9 AM](time)
- please wake me up [tonight](date) at [9 PM](time)
- set an alarm on [sunday](date) at [5 PM](time)
- wake me up at [6 AM](time) [today](date) 
- set an alarm at [7 PM](time) [today](date) 
- I want to wake up at [7 AM](time) [tomorrow](date) 
- can you wake me up at [9 AM](time) [tomorrow](date) 
- please wake me up at [9 PM](time) [tonight](date) 
- set an alarm on at [5 PM](time) [sunday](date) 

## intent:recommendations for food restaurants
- recommend me a food restaurant
- recommend a food place
- suggest a food restaurant
- suggestion for a food place
- what is the best food restaurants here?
- give me a list of food restaurants
- i am hungry
- i want to eat
- recommend me a [italian](food) food restaurant
- recommend a [indian](food) food place
- suggest a [indian](food) food restaurant
- suggestion for [indian](food) food place
- i want to eat [indian](food) food

## intent:play music/ audio book
- play [suger](music_name)
- play me [single ladies](music_name)
- play audio book [harry potter](music_name)
- play [maze runner](music_name) from audio book
- play music
- start music
- read me [Alchemist](music_name)


## intent:type of food
- [italian](food)
- [spanish](food)
- [oriental](food)
- [indian](food)
- [chinese](food)

## intent:Date
- [sunday](date)
- [monday](date)
- [tuesday](date)
- [wednesday](date)
- [thursday](date)
- [friday](date)
- [saturday](date)
- [tomorrow](date)
- [today](date)

