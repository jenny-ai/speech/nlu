# -*- coding: utf-8 -*-
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import logging
import requests
import json
from rasa_core_sdk import Action
from rasa_core_sdk.forms import FormAction, REQUESTED_SLOT
from rasa_core_sdk.events import SlotSet

logger = logging.getLogger(__name__)

"""
class ActionRecommendFood(Action):
    def name(self):
        # define the name of the action which can then be included in training stories
        return "action_recommend_food"

    def run(self, dispatcher, tracker, domain):
        # what your action should do
        
        food = tracker.get_slot("food")
        response = get_food(food)
        dispatcher.utter_message(response)  # send the message back to the user
        return []
"""

class FoodForm(FormAction):
   """Example of a custom form action"""

   def name(self):
       # type: () -> Text
       """Unique identifier of the form"""

       return "food_form"

   @staticmethod
   def required_slots(tracker):
       # type: () -> List[Text]
       """A list of required slots that the form has to fill"""

       return ["food"]

   def submit(self, dispatcher, tracker, domain):
       # what your action should do
        food = tracker.get_slot("food")
        response = get_food(food)
        dispatcher.utter_message(response)  # send the message back to the user
        return [SlotSet("food", None)]







class ActionJoke(Action):
    def name(self):
        # define the name of the action which can then be included in training stories
        return "action_joke"

    def run(self, dispatcher, tracker, domain):
        # what your action should do
        response = get_joke()
        dispatcher.utter_message(response)  # send the message back to the user
        return []

class ActionNotJoke(Action):
    def name(self):
        # define the name of the action which can then be included in training stories
        return "action_not_joke"

    def run(self, dispatcher, tracker, domain):
        # what your action should do
        response = get_not_joke()
        dispatcher.utter_message(response)  # send the message back to the user
        return []

"""
class ActionWakeUp(Action):
    def name(self):
        # define the name of the action which can then be included in training stories
        return "action ask to wake up at a certain time"

    def run(self, dispatcher, tracker, domain):
        # what your action should do
        date = tracker.get_slot("date")
        time = tracker.get_slot("time")
        response = get_wake(date, time)
        dispatcher.utter_message(response)  # send the message back to the user
        return []
"""

class WakeForm(FormAction):
   """Example of a custom form action"""

   def name(self):
       # type: () -> Text
       """Unique identifier of the form"""

       return "wake_form"

   @staticmethod
   def required_slots(tracker):
       # type: () -> List[Text]
       """A list of required slots that the form has to fill"""

       return ["time", "day"]

   def submit(self, dispatcher, tracker, domain):
       # what your action should do
        day = tracker.get_slot("day")
        time = tracker.get_slot("time")
        response = get_wake(day, time)
        dispatcher.utter_message(response)  # send the message back to the user
        return []


class ActionWakeUpAffirm(Action):
    def name(self):
        # define the name of the action which can then be included in training stories
        return "action affirm wake"

    def run(self, dispatcher, tracker, domain):
        # what your action should do
        day = tracker.get_slot("day")
        time = tracker.get_slot("time")
        response = get_affirm_wake(day, time)
        dispatcher.utter_message(response)  # send the message back to the user
        return [SlotSet("time", None),SlotSet("day", None)]

class ActionWakeUpDeny(Action):
    def name(self):
        # define the name of the action which can then be included in training stories
        return "action deny wake"

    def run(self, dispatcher, tracker, domain):
        # what your action should do
        response = get_deny_wake()
        dispatcher.utter_message(response)  # send the message back to the user
        return [SlotSet("time", None),SlotSet("day", None)]


"""
class ActionPlayMusic(Action):
    def name(self):
        # define the name of the action which can then be included in training stories
        return "action_play_music"

    def run(self, dispatcher, tracker, domain):
        # what your action should do
        music = tracker.get_slot("music_name")
        response = get_music(music)
        dispatcher.utter_message(response)  # send the message back to the user
        return []
"""

class MusicForm(FormAction):
   """Example of a custom form action"""

   def name(self):
       # type: () -> Text
       """Unique identifier of the form"""

       return "music_form"

   @staticmethod
   def required_slots(tracker):
       # type: () -> List[Text]
       """A list of required slots that the form has to fill"""

       return ["music_name"]

   def submit(self, dispatcher, tracker, domain):
       # what your action should do
        music = tracker.get_slot("music_name")
        response = get_music(music)
        dispatcher.utter_message(response)  # send the message back to the user
        return [SlotSet("music_name", None)]

class ActionTurnOnTV(Action):
    def name(self):
        # define the name of the action which can then be included in training stories
        return "action_turnTVon"

    def run(self, dispatcher, tracker, domain):
        # what your action should do
        response = TurnOnIOT()
        dispatcher.utter_message(response)  # send the message back to the user
        return []

class ActionTurnOffTV(Action):
    def name(self):
        # define the name of the action which can then be included in training stories
        return "action_turnTVoff"

    def run(self, dispatcher, tracker, domain):
        # what your action should do
        response = TurnOffIOT()
        dispatcher.utter_message(response)  # send the message back to the user
        return []

class ActionTurnUpVol(Action):
    def name(self):
        # define the name of the action which can then be included in training stories
        return "action_increaseVol"

    def run(self, dispatcher, tracker, domain):
        # what your action should do
        response = increaseVolumeIOT()
        dispatcher.utter_message(response)  # send the message back to the user
        return []

class ActionTurnDownVol(Action):
    def name(self):
        # define the name of the action which can then be included in training stories
        return "action_decreaseVol"

    def run(self, dispatcher, tracker, domain):
        # what your action should do
        response = decreaseVolumeIOT()
        dispatcher.utter_message(response)  # send the message back to the user
        return []

class tripTimeForm(FormAction):
   """Example of a custom form action"""

   def name(self):
       # type: () -> Text
       """Unique identifier of the form"""

       return "tripTime_form"

   @staticmethod
   def required_slots(tracker):
       # type: () -> List[Text]
       """A list of required slots that the form has to fill"""

       return ["destination"]

   def submit(self, dispatcher, tracker, domain):
       # what your action should do
        dest = tracker.get_slot("destination")
        source = tracker.get_slot("source")
        response = estimateTripTime(source,dest)
        dispatcher.utter_message(response)  # send the message back to the user
        return [SlotSet("destination", None) , SlotSet("source", None)]


class ActionMorningText(Action):
    def name(self):
        # define the name of the action which can then be included in training stories
        return "action_morning"

    def run(self, dispatcher, tracker, domain):
        # what your action should do
        response = morningtext()
        dispatcher.utter_message(response)  # send the message back to the user
        return []

class ActionSkipMorning(Action):
    def name(self):
        # define the name of the action which can then be included in training stories
        return "action_skip_morning"

    def run(self, dispatcher, tracker, domain):
        # what your action should do
        response = skipMorning()
        dispatcher.utter_message(response)  # send the message back to the user
        return []

class ActionStopMorning(Action):
    def name(self):
        # define the name of the action which can then be included in training stories
        return "action_stop_morning"

    def run(self, dispatcher, tracker, domain):
        # what your action should do
        response = stopMorning()
        dispatcher.utter_message(response)  # send the message back to the user
        return []

class ActionEmergency(Action):
    def name(self):
        # define the name of the action which can then be included in training stories
        return "action_emergency"

    def run(self, dispatcher, tracker, domain):
        # what your action should do
        response = emergency()
        dispatcher.utter_message(response)  # send the message back to the user
        return []

class ActionWifipass(Action):
    def name(self):
        # define the name of the action which can then be included in training stories
        return "action_wifipass"

    def run(self, dispatcher, tracker, domain):
        # what your action should do
        response = getWIFIpass()
        dispatcher.utter_message(response)  # send the message back to the user
        return []


def get_wake(day, time):
    res = "you want me to set you an alarm at {0} {1}, do you confirm that?".format(time, day)
    return res

def get_affirm_wake(day, time):
    res = "i have set you an alarm at {0} {1}".format(time, day)
    return res

def get_deny_wake():
    res = "Please ask again"
    return res

def get_food(food):
    res = "I have sent you a list of {} resturants on your phone".format(food)
    return res

def get_joke():
    res = "Do you want to hear another joke?"
    return res

def get_not_joke():
    res = "Okay"
    return res

def get_music(music):
    res = "Playing {} .....".format(music)
    return res

def TurnOnIOT():
    res = "turning TV on"
    return res

def TurnOffIOT():
    res = "turning TV off"
    return res

def increaseVolumeIOT():
    res = "turning volume up"
    return res

def decreaseVolumeIOT():
    res = "turning volume down"
    return res

def estimateTripTime(source,dest):
    res = "get the trip time from {} to {}".format(source,dest)
    return res

def morningtext():
    res = "get morning text"
    return res

def skipMorning():
    res = "skipping"
    return res

def stopMorning():
    res = "stopping"
    return res

def emergency():
    res = "calling emergency"
    return res

def getWIFIpass():
    res = "getting password"
    return res


