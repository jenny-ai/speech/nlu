## intent:AskWIFIpassword
- tell me the wifi password
- wifi password
- let me know the wifi password
- what is the wifi password
- the password of wifi network
- password to internet connection
- wifi key
- can you give the wifi password

## intent:Day
- [sunday](day)
- [monday](day)
- [tuesday](day)
- [wednesday](day)
- [thursday](day)
- [friday](day)
- [saturday](day)
- [tomorrow](day)
- [today](day)

## intent:Time
- [1 AM](time)
- [2 AM](time)
- [3 AM](time)
- [4 AM](time)
- [5 AM](time)
- [6 AM](time)
- [7 AM](time)
- [8 AM](time)
- [9 AM](time)
- [10 AM](time)
- [11 AM](time)
- [12 AM](time)
- [1 PM](time)
- [2 PM](time)
- [3 PM](time)
- [4 PM](time)
- [5 PM](time)
- [6 PM](time)
- [7 PM](time)
- [8 PM](time)
- [9 PM](time)
- [10 PM](time)
- [11 PM](time)
- [12 PM](time)

## intent:TurnOnTV
- turn on TV
- turn on the television
- switch on TV
- i want to watch the TV
- play TV
- turn TV on
- start the TV
- turn on the television
- switch the television on
- start the television
- switch on the television
- turn the television on
- turn the TV on

## intent:affirm
- yes
- yes sure
- absolutely
- for sure
- yes yes yes
- definitely
- okay
- ok
- sure
- yes sure
- yes
- yes
- yes
- yes
- yes
- yes
- yep
- okay
- ok
- yeah
- yes
- yes
- yes
- ok
- yes

## intent:ask to wake up at a certain time
- wake me up [today](day) at [6 AM](time)
- set an alarm [today](day) at [7 PM](time)
- I want to wake up [tomorrow](day) at [7 AM](time)
- can you wake me up [tomorrow](day) at [9 AM](time)
- please wake me up [tonight](day) at [9 PM](time)
- set an alarm on [sunday](day) at [5 PM](time)
- wake me up at [6 AM](time) [today](day)
- set an alarm at [7 PM](time) [today](day)
- I want to wake up at [7 AM](time) [tomorrow](day)
- can you wake me up at [9 AM](time) [tomorrow](day)
- please wake me up at [9 PM](time) [tonight](day)
- set an alarm on at [5 PM](time) [sunday](day)
- wake me up at [9 pm](time) [this night](day:today)

## intent:decreaseVolume
- decrease the volume
- decrease the volume of the TV
- decrease the TV volume
- raise the TV volume
- reduce the volume of tv
- turn down the volume please
- lower the tv volume
- turn down the volume
- turn the TV down
- it's too loud
- it's too loud
- it's too loud
- less volume
- minimize volume
- volume down
- turn down the speakers
- I need the volume to go down

## intent:deny
- no
- i can't
- another time
- some other time
- deny
- don't
- i don't want to
- no
- no
- no
- no
- no
- i don't want to
- i don't want to
- i don't want to
- no that is enough

## intent:emergency
- help
- help help
- help help help
- help me
- danger
- i need help
- i am in danger
- help
- help
- help
- help
- danger
- danger
- danger

## intent:estimate_trip_time
- how much time would you expect to move from [cairo](source) to [notre dame](destination)
- what is the estimated time to go from [sydney opera house](source) to [queen victoria building](destination)
- how long does it take from [cairo](source) to [the egyptian museum](destination)
- how much time does it take from [alexandria](source) to [the pyramids](destination)
- i want to go from [paris](source) to [the river nile](destination) how much time i will arrive there
- what is the duration it will take from [Dubai](source) to reach [the egyptian musem](destination)
- What time does it take from [amsterdam](source) to go to [dream park](destination)
- can you tell me the estimated time to travel from [Budapest](source) to [city stars mall](destination)
- can you estimate the time it would take to get from [Tahrir square](source) to [Al azhar mosque](destination)
- how long does it take from [cairo](source) to [alex](destination)
- how much time does it take to reach [giza pyramids](destination)
- what is the estimated time to travel to [barcelona](destination)
- how much time does it take to get to [the egyptian museum](destination)

## intent:goodbye
- Bye
- Goodbye
- See you later
- Bye bot
- Goodbye friend
- bye
- bye for now
- catch you later
- gotta go
- See you
- goodnight
- have a nice day
- i'm off
- see you later alligator
- we'll speak soon

## intent:increaseVolume
- increase the volume
- raise the TV volume
- turn up the volume please
- increase the volume for the television
- increase the tv sound
- increase the television volume
- increase the TV volume
- volume up
- sound up
- increase the sound
- increase the volume
- increase the volume
- increase the volume
- i can't hear it
- i can't hear it
- i can't hear it
- i can't hear it
- i can't hear the tv
- i can't hear the tv
- i can't hear the tv
- i can't hear the tv
- turn up the volume

## intent:joke
- Can you tell me a joke?
- I would like to hear a joke
- Tell me a joke
- A joke please
- Tell me a joke please
- I would like to hear a joke
- I would loke to hear a joke, please
- Can you tell jokes?
- Please tell me a joke
- I need to hear a joke
- tell me a joke
- joke
- another joke
- give me a joke
- tell me something funny
- make me laugh
- I want to laugh
- tell me a joke

## intent:location
- alexandria
- cairo
- paris
- milan
- barcelona
- america
- holand
- hungrey
- budapest

## intent:morning_text
- morning
- good morning
- good day
- greetings
- hey
- hello
- good morning

## intent:play music/ audio book
- play [suger](music_name)
- play me [single ladies](music_name)
- play audio book [harry potter](music_name)
- play [maze runner](music_name) from audio book
- play music
- start music
- read me [Alchemist](music_name)

## intent:recommendations for food restaurants
- recommend me a food restaurant
- recommend a food place
- suggest a food restaurant
- suggestion for a food place
- what is the best food restaurants here?
- give me a list of food restaurants
- i am hungry
- i want to eat
- recommend me a [italian](food) food restaurant
- recommend a [indian](food) food place
- suggest a [indian](food) food restaurant
- suggestion for [indian](food) food place
- i want to eat [indian](food) food

## intent:skip
- skip
- skip
- skip
- skip
- skip
- next
- next
- next
- next
- get the next
- what is the next
- skip this
- skip
- pass
- avoid
- skip
- skip
- skip

## intent:stop
- stop
- stop
- stop
- stop
- stop
- stop
- stop now
- please stop talking
- finish now
- terminate
- stop

## intent:thanks
- Thanks
- Thank you
- Thank you so much
- Thanks bot
- Thanks for that
- cheers
- cheers bro
- ok thanks!
- perfect thank you
- thanks a bunch for everything
- thanks for the help
- thanks a lot
- amazing, thanks
- cool, thanks
- cool thank you

## intent:turnoffTV
- turn off TV
- turn off the television
- switch off TV
- shutdown TV
- turn TV off
- shut the tv off
- switch off the television
- switch off the TV
- turn the television off
- shut down the television
- shut television off

## intent:type of food
- [italian](food)
- [spanish](food)
- [oriental](food)
- [indian](food)
- [chinese](food)

## synonym:today
- this day
- this morning
- this afternoon
- this night
