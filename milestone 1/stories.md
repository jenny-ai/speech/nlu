 
## story_goodbye
* goodbye
 - utter_goodbye

## story_thanks
* thanks
 - utter_thanks
 
## story_affirm
* affirm
 - utter_happy

## story_deny
* deny
 - utter_denyresponse

## story_joke_01
* joke
 - action_joke
* affirm
 - action_joke

## story_joke_02
* joke
 - action_joke
* deny
 - action_not_joke
 - action_restart

## Generated Story_joke_loop_03
* joke
    - action_joke
* affirm
    - action_joke
* affirm
    - action_joke
* affirm
    - action_joke
* affirm
    - action_joke
* deny
    - action_not_joke
    - action_restart


## story_ask_to_wake_up_01
* ask to wake up at a certain time
 - wake_form
 - form{"name": "wake_form"}
 - form{"name": null}
* affirm
 - action affirm wake

## story_ask_to_wake_up_02
* ask to wake up at a certain time
 - wake_form
 - form{"name": "wake_form"}
 - form{"name": null}
* deny
 - action deny wake

## Generated Story ask_to_wake_up_03_hapypath
* ask to wake up at a certain time{"time": "9 pm", "day": "today"}
    - slot{"day": "today"}
    - slot{"time": "9 pm"}
    - wake_form
    - form{"name": "wake_form"}
    - slot{"time": "9 pm"}
    - slot{"day": "today"}
    - slot{"time": "9 pm"}
    - slot{"day": "today"}
    - form{"name": null}
    - slot{"requested_slot": null}
* affirm
    - action affirm wake
    - slot{"time": null}
    - slot{"day": null}
    - action_restart

## story_recommendations_for_food_restaurants_01
* recommendations for food restaurants
 - food_form
 - form{"name": "food_form"}
 - form{"name": null}

## story_play_music_01
* play music/ audio book
 - music_form
 - form{"name": "music_form"} 
 - form{"name": null}

## story_TurningTVON_01_happypath
* TurnOnTV
 - action_turnTVon

## story_turningTVOFF_01_happypath
* turnoffTV
 - action_turnTVoff

## story_IncreaseTVvolume_01_happypath
* increaseVolume
 - action_increaseVol

## story_DecreaseTVvolume_01_happypath
* decreaseVolume
 - action_decreaseVol



## Generated StoryEstmatingTripTime_01_happypath
* estimate_trip_time{"source": "cairo", "destination": "alexandria"}
    - slot{"destination": "alexandria"}
    - slot{"source": "cairo"}
    - tripTime_form
    - form{"name": "tripTime_form"}
    - slot{"source": "cairo"}
    - slot{"destination": "alexandria"}
    - slot{"source": "cairo"}
    - slot{"destination": "alexandria"}
    - slot{"destination": null}
    - slot{"source": null}
    - form{"name": null}
    - slot{"requested_slot": null}
    - action_restart

## Generated Story 8889903771210993236
* estimate_trip_time{"source": "cairo", "destination": "alex"}
    - slot{"destination": "alex"}
    - slot{"source": "cairo"}
    - tripTime_form
    - form{"name": "tripTime_form"}
    - slot{"source": "cairo"}
    - slot{"destination": "alex"}
    - slot{"source": "cairo"}
    - slot{"destination": "alex"}
    - slot{"destination": null}
    - slot{"source": null}
    - form{"name": null}
    - slot{"requested_slot": null}
    - action_restart

## Generated Story 1032739621866524291
* estimate_trip_time{"destination": "giza pyramids"}
    - slot{"destination": "giza pyramids"}
    - tripTime_form
    - form{"name": "tripTime_form"}
    - slot{"destination": "giza pyramids"}
    - slot{"destination": "giza pyramids"}
    - slot{"destination": null}
    - slot{"source": null}
    - form{"name": null}
    - slot{"requested_slot": null}
    - action_restart

## Generated Story -1495241321911853077
* estimate_trip_time{"destination": "barcelona"}
    - slot{"destination": "barcelona"}
    - tripTime_form
    - form{"name": "tripTime_form"}
    - slot{"destination": "barcelona"}
    - slot{"destination": "barcelona"}
    - slot{"destination": null}
    - slot{"source": null}
    - form{"name": null}
    - slot{"requested_slot": null}
    - action_restart

## Generated Story -2567320375339292926
* estimate_trip_time{"destination": "the egyptian museum"}
    - slot{"destination": "the egyptian museum"}
    - tripTime_form
    - form{"name": "tripTime_form"}
    - slot{"destination": "the egyptian museum"}
    - slot{"destination": "the egyptian museum"}
    - slot{"destination": null}
    - slot{"source": null}
    - form{"name": null}
    - slot{"requested_slot": null}
    - action_restart

## story_morningtext_01_happypath
* morning_text
 - action_morning

## story_morningtext_02_happypathskip
* morning_text
 - action_morning
* skip
 - action_skip_morning


## story_morningtext_03_happypathskip
* morning_text
 - action_morning
* skip
 - action_skip_morning
* skip
 - action_skip_morning

## story_morningtext_03_happypath
* morning_text
 - action_morning
* skip
 - action_skip_morning
* skip
 - action_skip_morning
* stop
 - action_stop_morning
 - action_restart

## Generated Story_morningtext_04_happypath
* morning_text
    - action_morning
* skip
    - action_skip_morning
* skip
    - action_skip_morning
* skip
    - action_skip_morning
* stop
    - action_stop_morning
    - action_restart


## story_morningtext_04_happypath
* morning_text
 - action_morning
* stop
 - action_stop_morning
 - action_restart

## story_emergency_01_happypath
* emergency
 - action_emergency

## story_askwifipass_01_happypath
* AskWIFIpassword
 - action_wifipass






