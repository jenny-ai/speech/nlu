# -*- coding: utf-8 -*-
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import logging
import requests
import json
from rasa_core_sdk import Action
from rasa_core_sdk.forms import FormAction, REQUESTED_SLOT
from rasa_core_sdk.events import SlotSet

logger = logging.getLogger(__name__)

"""
class ActionRecommendFood(Action):
    def name(self):
        # define the name of the action which can then be included in training stories
        return "action_recommend_food"

    def run(self, dispatcher, tracker, domain):
        # what your action should do
        food = tracker.get_slot("food")
        response = get_food(food)
        dispatcher.utter_message(response)  # send the message back to the user
        return []
"""

class FoodForm(FormAction):
    """Example of a custom form action"""

    def name(self):
        # type: () -> Text
        """Unique identifier of the form"""
        return "food_form"

    @staticmethod
    def required_slots(tracker):
        # type: () -> List[Text]
        """A list of required slots that the form has to fill"""

        return ["food"]

    def submit(self, dispatcher, tracker, domain):
        # what your action should do
        food = tracker.get_slot("food")
        response = "{"+'"utter" : "False", "rasaFlag" : "False","fn" :"get_food","food" : "{}"'.format(food)+"}"
        dispatcher.utter_message(response)  # send the message back to the user
        return [SlotSet("food", None)]


class ActionJoke(Action):
    def name(self):
        # define the name of the action which can then be included in training stories
        return "action_joke"

    def run(self, dispatcher, tracker, domain):
        # what your action should do
        response = [True,"get_joke"]
        response = '{ "utter" : "False", "rasaFlag" : "True","fn" :"get_joke"}'
        dispatcher.utter_message(response)  # send the message back to the user
        return []

class ActionNotJoke(Action):
    def name(self):
        # define the name of the action which can then be included in training stories
        return "action_not_joke"

    def run(self, dispatcher, tracker, domain):
        # what your action should do
        response = '{ "utter" : "False", "rasaFlag" : "False","fn" : "get_not_joke"}'
        dispatcher.utter_message(response)  # send the message back to the user
        return []

"""
class ActionWakeUp(Action):
    def name(self):
        # define the name of the action which can then be included in training stories
        return "action ask to wake up at a certain time"

    def run(self, dispatcher, tracker, domain):
        # what your action should do
        date = tracker.get_slot("date")
        time = tracker.get_slot("time")
        response = get_wake(date, time)
        dispatcher.utter_message(response)  # send the message back to the user
        return []
"""

class WakeForm(FormAction):
    """Example of a custom form action"""

    def name(self):
        # type: () -> Text
        """Unique identifier of the form"""

        return "wake_form"

    @staticmethod
    def required_slots(tracker):
        # type: () -> List[Text]
        """A list of required slots that the form has to fill"""

        return ["time", "day"]

    def submit(self, dispatcher, tracker, domain):
        # what your action should do
         day = tracker.get_slot("day")
         time = tracker.get_slot("time")
         response = '{'+'"utter" : "False", "rasaFlag" : "True","fn" :"get_wake","day" : "{0}","time" : "{1}"'.format(day,time)+'}'
         #response = [True,"get_wake",day, time]
         dispatcher.utter_message(response)  # send the message back to the user
         return []


class ActionWakeUpAffirm(Action):
    def name(self):
        # define the name of the action which can then be included in training stories
        return "action affirm wake"

    def run(self, dispatcher, tracker, domain):
        # what your action should do
        day = tracker.get_slot("day")
        time = tracker.get_slot("time")
        response = '{'+ '"utter" : "False", "rasaFlag" : "False","fn" : "get_affirm_wake","day" : "{0}","time" : "{1}"'.format(day,time)+'}'
        #response = [True,"get_affirm_wake",day, time]
        dispatcher.utter_message(response)  # send the message back to the user
        return [SlotSet("time", None),SlotSet("day", None)]

class ActionWakeUpDeny(Action):
    def name(self):
        # define the name of the action which can then be included in training stories
        return "action deny wake"

    def run(self, dispatcher, tracker, domain):
        # what your action should do
        response = '{ "utter" : "False", "rasaFlag" : "False","fn":"get_deny_wake"}'
        #response = [True,"get_deny_wake"]
        dispatcher.utter_message(response)  # send the message back to the user
        return [SlotSet("time", None),SlotSet("day", None)]


"""
class ActionPlayMusic(Action):
    def name(self):
        # define the name of the action which can then be included in training stories
        return "action_play_music"

    def run(self, dispatcher, tracker, domain):
        # what your action should do
        music = tracker.get_slot("music_name")
        response = get_music(music)
        dispatcher.utter_message(response)  # send the message back to the user
        return []
"""

class MusicForm(FormAction):
    """Example of a custom form action"""

    def name(self):
        # type: () -> Text
        """Unique identifier of the form"""

        return "music_form"

    @staticmethod
    def required_slots(tracker):
        # type: () -> List[Text]
        """A list of required slots that the form has to fill"""

        return ["music_name"]

    def submit(self, dispatcher, tracker, domain):
        # what your action should do
        music = tracker.get_slot("music_name")
        response = '{'+ '"utter" : "False","rasaFlag" : "True","fn" : "get_music","music" : "{0}"'.format(music) +'}'
        #response = [True,"get_music",music]
        dispatcher.utter_message(response)  # send the message back to the user
        return [SlotSet("music_name", None)]

class ActionTurnOnTV(Action):
    def name(self):
        # define the name of the action which can then be included in training stories
        return "action_turnTVon"

    def run(self, dispatcher, tracker, domain):
        # what your action should do
        response = '{ "utter" : "False","rasaFlag" : "False","fn" : "TurnOnIOT"}'

        #response = [True,"TurnOnIOT"]
        dispatcher.utter_message(response)  # send the message back to the user
        return []

class ActionTurnOffTV(Action):
    def name(self):
        # define the name of the action which can then be included in training stories
        return "action_turnTVoff"

    def run(self, dispatcher, tracker, domain):
        # what your action should do
        response = '{"utter" : "False", "rasaFlag" : "False","fn" : "TurnOffIOT"}'

        #response = [True,"TurnOffIOT"]
        dispatcher.utter_message(response)  # send the message back to the user
        return []

class ActionTurnUpVol(Action):
    def name(self):
        # define the name of the action which can then be included in training stories
        return "action_increaseVol"

    def run(self, dispatcher, tracker, domain):
        # what your action should do
        response = '{ "utter" : "False","rasaFlag" : "False","fn" : "increaseVolumeIOT"}'

        #response = [True,"increaseVolumeIOT"]
        dispatcher.utter_message(response)  # send the message back to the user
        return []

class ActionTurnDownVol(Action):
    def name(self):
        # define the name of the action which can then be included in training stories
        return "action_decreaseVol"

    def run(self, dispatcher, tracker, domain):
        # what your action should do
        response = '{ "utter" : "False","rasaFlag" : "False","fn" : "decreaseVolumeIOT"}'

        #response = [True,"decreaseVolumeIOT"]
        dispatcher.utter_message(response)  # send the message back to the user
        return []

class tripTimeForm(FormAction):
    """Example of a custom form action"""

    def name(self):
        # type: () -> Text
        """Unique identifier of the form"""

        return "tripTime_form"

    @staticmethod
    def required_slots(tracker):
        # type: () -> List[Text]
        """A list of required slots that the form has to fill"""

        return ["place_location"]

    def submit(self, dispatcher, tracker, domain):
        # what your action should do
        dest = tracker.get_slot("place_location")
        response = '{' + '"utter" : "False","rasaFlag" : "False","fn" : "estimateTripTime","dest" : "{0}"'.format(dest) + '}'
        #response = [True,"estimateTripTime",dest]
        dispatcher.utter_message(response)  # send the message back to the user
        return [SlotSet("place_location", None)]


class ActionMorningText(Action):
    def name(self):
        # define the name of the action which can then be included in training stories
        return "action_morning"

    def run(self, dispatcher, tracker, domain):
        # what your action should do
        response = '{ "utter" : "False","rasaFlag" : "True","fn" : "morningtext"}'

        #response = [True,"morningtext"]
        dispatcher.utter_message(response)  # send the message back to the user
        return []

class ActionSkipMorning(Action):
    def name(self):
        # define the name of the action which can then be included in training stories
        return "action_skip_morning"

    def run(self, dispatcher, tracker, domain):
        # what your action should do
        response = '{ "utter" : "False","rasaFlag" : "True","fn" : "skipMorning"}'

        #response = [True,"skipMorning"]
        dispatcher.utter_message(response)  # send the message back to the user
        return []

class ActionStopMorning(Action):
    def name(self):
        # define the name of the action which can then be included in training stories
        return "action_stop_morning"

    def run(self, dispatcher, tracker, domain):
        # what your action should do
        response = '{ "utter" : "False","rasaFlag" : "False","fn" : "stopMorning"}'

        #response = [True,"stopMorning"]
        dispatcher.utter_message(response)  # send the message back to the user
        return []

class ActionEmergency(Action):
    def name(self):
        # define the name of the action which can then be included in training stories
        return "action_emergency"

    def run(self, dispatcher, tracker, domain):
        # what your action should do
        response = '{ "utter" : "False","rasaFlag" : "False","fn" : "emergency"}'

        #response = [True,"emergency"]
        dispatcher.utter_message(response)  # send the message back to the user
        return []

class ActionWifipass(Action):
    def name(self):
        # define the name of the action which can then be included in training stories
        return "action_wifipass"

    def run(self, dispatcher, tracker, domain):
        # what your action should do
        response = '{ "utter" : "False","rasaFlag" : "False","fn" : "getWIFIpass"}'

        #response = [True,"getWIFIpass"]
        dispatcher.utter_message(response)  # send the message back to the user
        return []

class providelocationForm(FormAction):
    """Example of a custom form action"""
    def name(self):
        # type: () -> Text
        """Unique identifier of the form"""
        return "providelocation_form"
    @staticmethod
    def required_slots(tracker):
        # type: () -> List[Text]
        """A list of required slots that the form has to fill"""
        return ["place_location"]
    def submit(self, dispatcher, tracker, domain):
        # what your action should do
        dest = tracker.get_slot("place_location")
        response = "{"+'"utter" : "False","rasaFlag" : "False","fn" : "provideLocation","dest" : "{}"'.format(dest) + "}"
        #response = [True,"provideLocation",dest]
        dispatcher.utter_message(response)  # send the message back to the user
        return [SlotSet("place_location", None)]

class recommendationForm(FormAction):
    """Example of a custom form action"""

    def name(self):
        # type: () -> Text
        """Unique identifier of the form"""

        return "recommendation_form"

    @staticmethod
    def required_slots(tracker):
        # type: () -> List[Text]
        """A list of required slots that the form has to fill"""

        return ["recommended_place"]

    def submit(self, dispatcher, tracker, domain):
        # what your action should do
        dest = tracker.get_slot("recommended_place")
        response = '{'+ '"utter" : "False","rasaFlag" : "False","fn" : "placeRecommendation","slot1" : "{0}"'.format(dest)+'}'

        #response = [True,"placeRecommendation",dest]
        dispatcher.utter_message(response)  # send the message back to the user
        return [SlotSet("recommended_place", None)]

class ActionRemindEvent(Action):
    def name(self):
        # define the name of the action which can then be included in training stories
        return "remind_event"

    def run(self, dispatcher, tracker, domain):
        # what your action should do
        response = '{"utter" : "False", "rasaFlag" : "True","fn" : "remindEvent"}'

        #response = [True,"remindEvent"]
        dispatcher.utter_message(response)  # send the message back to the user
        return []

class ActionSkipRemindEvent(Action):
    def name(self):
        # define the name of the action which can then be included in training stories
        return "skip_remind_event"  
    def run(self, dispatcher, tracker, domain):
        # what your action should do
        response = '{ "utter" : "False","rasaFlag" : "True","fn" : "skip_remind_event"}'
        #response = [True,"skip_remind_event"]
        dispatcher.utter_message(response)  # send the message back to the user
        return []
class ActionHotelSounds(Action):
    def name(self):
        # define the name of the action which can then be included in training stories
        return "hotel_sounds"
    def run(self, dispatcher, tracker, domain):
        # what your action should do
        response = '{ "utter" : "False","rasaFlag" : "True","fn" : "get_hotel_sounds_playlist"}'
        #response = [True,"get_hotel_sounds_playlist"]
        dispatcher.utter_message(response)  # send the message back to the user
        return []
class ActionSkipHotelSounds(Action):
    def name(self):
        # define the name of the action which can then be included in training stories
        return "skip_hotel_sounds"
    def run(self, dispatcher, tracker, domain):
        # what your action should do
        response = '{ "utter" : "False","rasaFlag" : "True","fn" : "skip_hotel_sounds"}'
        #response = [True,"skip_hotel_sounds"]
        dispatcher.utter_message(response)  # send the message back to the user
        return []

class ActionStopHotelSounds(Action):
    def name(self):
        # define the name of the action which can then be included in training stories
        return "stop_hotel_sounds"

    def run(self, dispatcher, tracker, domain):
        # what your action should do
        response = '{ "utter" : "False","rasaFlag" : "False","fn" : "stop_hotel_sounds"}'

        #response = [True,"stop_hotel_sounds"]
        dispatcher.utter_message("stop hotel sounds")  # send the message back to the user
        return []
class ActionStopRemindEvent(Action):
    def name(self):
        # define the name of the action which can then be included in training stories
        return "stop_remind_event"
    def run(self, dispatcher, tracker, domain):
        # what your action should do
        response = '{ "utter" : "False","rasaFlag" : "False","fn" : "stop_remind_event"}'
        #response = [True,"stop_remind_event"]
        dispatcher.utter_message(response)  # send the message back to the user
        return []
"""def slot_mappings(self):
        # type: () -> Dict[Text: Union[Dict, List[Dict]]]

        return {"place_location":[self.from_entity(entity="place_location"), self.from_intent(intent='estimate_trip_time',value=False),self.from_intent(intent='provide_location',value=True)]}
"""
