## intent:AskWIFIpassword
- tell me the wifi password
- wifi password
- let me know the wifi password
- what is the wifi password
- the password of wifi network
- password to internet connection
- wifi key
- wi fi key
- wi fi password
- password of wi fi
- access password of wi fi
- authorized wi fi key
- get me authorized password of wifi
- can you give the wifi password
- wifi
- wi fi
- wifi pass
- wi fi pass
- wifi password
- wi fi
- wifi pass

## intent:Day
- [sunday](day)
- [monday](day)
- [tuesday](day)
- [wednesday](day)
- [thursday](day)
- [friday](day)
- [saturday](day)
- [tomorrow](day)
- [today](day)

## intent:EventReminder
- what are today events
- remind me my coming events
- can i get my event schedule
- i am bored what events can i attend
- i forget my events tell them to me again
- tell me upcoming events
- what i have in upcoming events
- upcoming events
- remind me with events
- events
- upcoming events
- schedule
- do i have anything to attend in my plan
- review with me my schedule
- review me my events
- i want to check out my events
- can i check my schedule for today
- get me my events
- get me my added events
- i think i have to attend an event remind me of it
- do i have any events
- what events should i attend
- let me know my events
- what are today events
- remind me my comming events
- get events
- what events i have

## intent:Time
- [1 AM](time)
- [2 AM](time)
- [3 AM](time)
- [4 AM](time)
- [5 AM](time)
- [6 AM](time)
- [7 AM](time)
- [8 AM](time)
- [9 AM](time)
- [10 AM](time)
- [11 AM](time)
- [12 AM](time)
- [1 PM](time)
- [2 PM](time)
- [3 PM](time)
- [4 PM](time)
- [5 PM](time)
- [6 PM](time)
- [7 PM](time)
- [8 PM](time)
- [9 PM](time)
- [10 PM](time)
- [11 PM](time)
- [12 PM](time)

## intent:TurnOnTV
- turn on TV
- turn on the television
- switch on TV
- i want to watch the TV
- play TV
- turn TV on
- start the TV
- turn on the television
- switch the television on
- start the television
- switch on the television
- turn the television on
- turn the TV on
- open the tv
- set the television on
- get the television open
- can you open the television
- would you switch the tv on
- tv on
- tv open
- television open
- watch television
- switch on tv
- turn on the tv

## intent:affirm
- yes
- yes sure
- absolutely
- for sure
- yes yes yes
- definitely
- okay
- ok
- sure
- yes sure
- yes
- yes
- yes
- yes
- yes
- yes
- yep
- okay
- ok
- yeah
- yes
- yes
- yes
- ok
- yes
- yes
- yes
- ok

## intent:ask to wake up at a certain time
- wake me up [today](day) at [6 AM](time)
- set an alarm [today](day) at [7 PM](time)
- I want to wake up [tomorrow](day) at [7 AM](time)
- can you wake me up [tomorrow](day) at [9 AM](time)
- please wake me up [tonight](day:today) at [9 PM](time)
- set an alarm on [sunday](day) at [5 PM](time)
- wake me up at [6 AM](time) [today](day)
- set an alarm at [7 PM](time) [today](day)
- I want to wake up at [7 AM](time) [tomorrow](day)
- can you wake me up at [9 AM](time) [tomorrow](day)
- please wake me up at [9 PM](time) [tonight](day:today)
- set an alarm on at [5 PM](time) [sunday](day)
- wake me up at [9 pm](time) [this night](day:today)
- get me waked up at [10 am](time)
- check that i am awaken at [2 pm](time)[sunday](day)
- can you handle an alarm by [1 am](time) to revise for meeting
- alarm me [today](day) at [noon](time:12 pm)
- alarm at [10 am](time)
- waked up at [midnight](time:12 am)
- inform me at [3 pm](time)
- set my alarm [tomorrow](day) at [1 pm](time)
- would you set alarm by [9 am]
- i am very tired can you wake me at [8 pm](time)[this day](day:today)
- handle that being waked up by [5 pm](time)

## intent:decreaseVolume
- decrease the volume
- decrease the volume of the TV
- decrease the TV volume
- reduce the volume of tv
- turn down the volume please
- lower the tv volume
- turn down the volume
- turn the TV down
- it's too loud
- it's much noise
- i can't hear my friend lower the music volume
- less volume
- minimize volume
- volume down
- turn down the speakers
- I need the volume to go down
- set volume low
- make it more quiet
- what a noise
- i got a headache quiet sounds please
- can i get volume lower
- bring volume down
- turn on loud mode
- quiet the room

## intent:deny
- no
- i can't
- another time
- some other time
- deny
- don't
- i don't want to
- no
- no
- no
- nope
- nope
- no
- i don't want to
- i don't want to
- i don't want to
- no that is enough
- no

## intent:emergency
- help
- help help
- help help help
- help me
- danger
- i need help
- i am in danger
- help me
- help
- help
- help
- in danger
- danger
- danger
- sos
- emergency
- call the ambulance
- quickly call the police
- it's emergency call the reception
- i can't breathe
- i fell down get me the help
- i am afraid there is strang sound in the room
- get me help
- rescue me
- i need a help emergently

## intent:estimate_trip_time
- how much time would you expect to move from here to [notre dame](place_location)
- what is the estimated time to go  to [queen victoria building](place_location)
- how long does it take to [the egyptian museum](place_location)
- how much time does it take to [the pyramids](place_location)
- i want to go to [the river nile](place_location) how much time i will arrive there
- what is the duration it will take to reach [the egyptian musem](place_location)
- What time does it take to go to [dream park](place_location)
- can you tell me the estimated time to travel to [city stars mall](place_location)
- can you estimate the time it would take to get  to [Al azhar mosque](place_location)
- how long does it take to [alex](place_location)
- how much time does it take to reach [giza pyramids](place_location)
- what is the estimated time to travel to [barcelona](place_location)
- how much time does it take to get to [the egyptian museum](place_location)
- what time to reach [dubai](place_location)
- time to go to [athena](place_location)
- check time spent to [Tahiti](place_location)
- time does it take to [Phuket](place_location)
- spent time to reach [maui](place_location)
- duration to get [disney land](place_location)
- how much time should i spend to arrive at [eiffel](place_location)
- inform me the estimated duration to get to [bora bora](place_location)
- period of time to arrive [Grand Canyon](place_location)
- can i get estimated time to [Yosemite](place_location)
- estimated time to [Machu Picchu](place_location)
- time duration for [Banff](place_location)
- what is the time to go to [giza zoo](place_location)

## intent:goodbye
- Bye
- Goodbye
- See you later
- Bye bot
- Goodbye friend
- bye
- bye for now
- catch you later
- gotta go
- See you
- goodnight
- have a nice day
- i'm off
- see you later alligator
- we'll speak soon
- bye
- goodbye

## intent:hotel_music
- tell me the hotel playlist
- i heard marvellous music down stairs can i get the playlist
- share me hotel playlist
- tell me the hotel playlist
- i heard marvellous music down stairs can i get the playlist
- share me hotel playlist
- hotel playlist
- i want hotel playlist
- i want to listen hotel playlist
- get me hotel playlist
- hotel playlist
- downstairs music playlist
- hall music playlist
- hotel music
- get me played playlist
- i want songs names of the hotel
- search me songs played by hotel
- can i reach to music playlist of the hotel
- share with me hotel selected playlist
- hotel selected music
- hotel selected playlist
- can you get me playlist played in hotel
- list me hotel selected sounds
- name me songs in hotel playlist
- play hotel sounds

## intent:increaseVolume
- increase the volume
- raise the TV volume
- turn up the volume please
- increase the volume for the television
- increase the tv sound
- increase the television volume
- increase the TV volume
- volume up
- sound up
- increase the sound
- increase the volume
- increase the volume
- increase the volume
- i can't hear it
- i can't hear it
- i can't hear it
- i can't hear it
- i can't hear the tv
- i can't hear the tv
- i can't hear the tv
- i can't hear the tv
- turn up the volume
- turn up the volume please
- turn up the volume please
- turn up the volume please

## intent:joke
- Can you tell me a joke?
- I would like to hear a joke
- Tell me a joke
- A joke please
- Tell me a joke please
- I would like to hear a joke
- I would loke to hear a joke, please
- Can you tell jokes?
- Please tell me a joke
- I need to hear a joke
- tell me a joke
- joke
- another joke
- give me a joke
- tell me something funny
- make me laugh
- I want to laugh
- tell me a joke
- joke
- joke
- joke
- tell me a joke

## intent:location
- [alexandria](place_location)
- [cairo](place_location)
- [paris](place_location)
- [milan](place_location)
- [america](place_location)
- [barcelona](place_location)
- [holand](place_location)
- [hungrey](place_location)
- [budapest](place_location)
- [cairo tower](place_location)
- [giza pyramides](place_location)
- [cairo](place_location)
- [shoubra](place_location)

## intent:morning_text
- morning
- good morning
- good day
- greetings
- good morning
- morning
- good morning jenny
- morning jenny
- morning
- morning

## intent:place_recommendation
- would you recommend me [museum](recommended_place) to visit
- what is the best [cinema](recommended_place) around here
- recommend me [gym](recommended_place) to go
- tell me what is the best [club](recommended_place) near the hotel
- give me a recommendation about [shopping mall](recommended_place)
- which [mall](recommended_place) has the highest ratings
- can you tell me where i can find [spa](recommended_place)
- would you recommend me [club](recommended_place) to visit
- what is the best [spa](recommended_place) around here
- recommend me [cinema](recommended_place) to go
- tell me what is the best [gym](recommended_place) near the hotel
- give me a recommendation about [mall](recommended_place)
- which [spa](recommended_place) has the highest ratings
- can you tell me where i can find [museum](recommended_place)

## intent:play music/ audio book
- play [suger](music_name)
- play me [single ladies](music_name)
- play audio book [harry potter](music_name)
- play [maze runner](music_name) from audio book
- play music
- start music
- read me [Alchemist](music_name)

## intent:provide_location
- where is [the nearest bank](place_location)
- how do you get to [alberta](place_location)
- what is the location of [sydney opera house](place_location)
- how can i go to [the egyptian museum ](place_location)
- send me the location of [the river nile](place_location)
- please i want to know the location of [Giza pyramids](place_location)
- tell me where is [carrefour](place_location)
- i want to know the location of [cairo tower](place_location)
- show me the location of [egyptian museum](place_location)
- the location of [citadel of Qaitbay](place_location)
- give me the location of [the hanging church](place_location)
- tell me the location of [AlAzhar park](place_location)
- i want to know the location of [mosque of Muhammad Ali](place_location) on the map
- where is [Statue of Liberty](place_location) on the map
- show me the path to [arc de triomphe](place_location)
- location
- get location
- what is the location
- please i want to go to [anne frank house](place_location)
- show me a location
- how can i get this location
- where is [arc de triomphe](place_location)
- location
- get location
- give me a location

## intent:recommendations for food restaurants
- recommend me a food restaurant
- recommend a food place
- suggest a food restaurant
- suggestion for a food place
- what is the best food restaurants here?
- give me a list of food restaurants
- i am hungry
- i want to eat
- recommend me a [italian](food) food restaurant
- recommend a [indian](food) food place
- suggest a [indian](food) food restaurant
- suggestion for [indian](food) food place
- i want to eat [indian](food) food
- i want to eat
- recommend me a food restaurant
- suggest a food resturant

## intent:skip
- skip
- skip
- skip
- skip
- skip
- next
- next
- next
- next
- get the next
- what is the next
- skip this
- skip
- pass
- avoid
- skip
- skip
- skip
- skip
- skip
- skip

## intent:stop
- stop
- stop
- stop
- stop
- stop
- stop
- stop now
- please stop talking
- finish now
- terminate
- stop
- stop music
- stop

## intent:thanks
- Thanks
- Thank you
- Thank you so much
- Thanks bot
- Thanks for that
- cheers
- cheers bro
- ok thanks!
- perfect thank you
- thanks a bunch for everything
- thanks for the help
- thanks a lot
- amazing, thanks
- cool, thanks
- cool thank you

## intent:turnoffTV
- turn off TV
- turn off the television
- switch off TV
- shutdown TV
- turn TV off
- shut the tv off
- switch off the television
- switch off the TV
- turn the television off
- shut down the television
- shut television off

## intent:type of food
- [italian](food)
- [spanish](food)
- [oriental](food)
- [indian](food)
- [chinese](food)
- [spanish](food)
- [asian](food)

## synonym:12 am
- midnight

## synonym:12 pm
- noon

## synonym:today
- tonight
- this night
- this day
- this morning
- this afternoon

## lookup:place_location
- cairo
- Alex
- Alexandria
- newyork
- Ainshams
- Elabaseya
- Abdo basha
- ain shams university
- cairo university
- giza zoo