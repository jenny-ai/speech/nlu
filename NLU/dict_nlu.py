import json
from NLU.action_methods import *
from NLU.RasaServer import sendcommand
from NLU.RasaServer import reply

name_action_dict = {
    "get_food":get_food,
    "get_joke":get_joke,
    "get_not_joke":get_not_joke,
    "get_wake":get_wake,
    "get_affirm_wake":get_affirm_wake,
    "get_deny_wake":get_deny_wake,
    "get_music":get_music,
    "TurnOnIOT":TurnOnIOT,
    "TurnOffIOT":TurnOffIOT,
    "increaseVolumeIOT":increaseVolumeIOT,
    "decreaseVolumeIOT":decreaseVolumeIOT,
    "estimateTripTime":estimateTripTime,
    "morningtext":morningtext,
    "skipMorning":skipMorning,
    "stopMorning":stopMorning,
    "emergency":emergency,
    "getWIFIpass":getWIFIpass,
    "provideLocation":provideLocation,
    "placeRecommendation":placeRecommendation,
    "remindEvent":remindEvent,
    "skip_remind_event":skip_remind_event,
    "get_hotel_sounds_playlist":get_hotel_sounds_playlist,
    "skip_hotel_sounds":skip_hotel_sounds,
    "stop_hotel_sounds":stop_hotel_sounds,
    "stop_remind_event":stop_remind_event
}

def askjenny(cmd_source,command):
    """
    take a command string and the source of this command to get a response
    inputs :
     - cmd source : False, the command comes from a voice source
                  : True , the command comes from the app
     - command : strig which contains what the user say

    """
    response_json = sendcommand(command)
    """respose data contains :
            1. utter flag : true if utterance response
            2. Rasa flag : true if the assistant wait for a reply
            3. response msg : utter_message   ..... exists when utter = True
            4. fn : function name to call it
    """
    
    for res_json in response_json :
        if('{' not in res_json):
            res_json = '{'+res_json+'}'
        res_json = json.loads(res_json)
        if res_json["utter"] == "True":

            reply(cmd_source,res_json["response"])
            rasaFlag = res_json["rasaFlag"]
        else :
            response = name_action_dict[res_json["fn"]](cmd_source,res_json)
            rasaFlag = res_json["rasaFlag"]
    return rasaFlag


