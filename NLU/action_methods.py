from NLU.RasaServer import reply
from jenny.assistant.joke import get_joke
from jenny.assistant.restaurants_recommendation import recommend_restaurants
from jenny.assistant.Alarm.add_alarm import add_alarm
from jenny.assistant.estimate_trip_time import estimate_trip_time
from jenny.assistant.morningtext import morning_text_form
from jenny.assistant.send_maps_url_to_app import send_maps_url_to_app
from jenny.assistant.places_recommendation import places_recommendation_with_keyword
from jenny.assistant.events_funs import coming_events






def get_food(cmd_source,rasa_message_list):
    food = rasa_message_list["food"]
    res = "I will get a list of {} resturants for you".format(food)
    reply(cmd_source,res)
    list_of_resturants = recommend_restaurants(food_type = food)
    if list_of_resturants == False :
        reply(cmd_source,"i am so sorry i can't get it")
    response = "here are some of {} resutrants ".format(food)
    for resturant in list_of_resturants :
        response = response + resturant["name"] + " in " + resturant["location_address"] + " and "
    reply(cmd_source,response[:-5])

def get_joke(cmd_source,rasa_message_list):
    joke = get_joke()
    if joke == False :
        reply(cmd_source,"i am sorry i haven't any jokes now")
    reply(cmd_source,joke)
    reply(cmd_source,"Do you want to hear another joke?")


def get_not_joke(cmd_source,rasa_message_list):
    res = "Okay as you like"
    reply(cmd_source,res)

def get_wake(cmd_source,rasa_message_list):
    day = rasa_message_list["day"]
    time = rasa_message_list["time"]
    res = "Do you want me to set you an alarm at {0} {1}, do you confirm that?".format(time, day)
    reply(cmd_source,res)

def get_affirm_wake(cmd_source,rasa_message_list):
    day = rasa_message_list["day"]
    time = rasa_message_list["time"]
    #have not finished yet
    if (add_alarm(time, alarm_txt, snoozing)):
        res = "i have set you an alarm at {0} {1}".format(time, day)
        reply(cmd_source,res)

def get_deny_wake(cmd_source,rasa_message_list):
    res = "okay"
    reply(cmd_source,res)


def get_music(cmd_source,rasa_message_list):
    music = rasa_message_list["music"]
    res = "I'm sorry , i am still learning, i can't play this music for you , you can play this music from your mobile application"
    reply(cmd_source,res)

def TurnOnIOT(cmd_source,rasa_message_list):
    reply(cmd_source,"Turning the Tv on")
    res = "unfortunately you can only control your TV from my application , i hope i can do that in the future"
    reply(cmd_source,res)

def TurnOffIOT(cmd_source,rasa_message_list):
    reply(cmd_source,"Turning the Tv off")
    res = "unfortunately you can only control your TV from my application , i hope i can do that in the future"
    reply(cmd_source,res)

def increaseVolumeIOT(cmd_source,rasa_message_list):
    reply(cmd_source,"Turning the volume up")
    res = "unfortunately you can only control your TV from my application , i hope i can do that in the future"
    reply(cmd_source,res)

def decreaseVolumeIOT(cmd_source,rasa_message_list):
    reply(cmd_source,"Turning the volume down")
    res = "unfortunately you can only control your TV from my application , i hope i can do that in the future"
    reply(cmd_source,res)

def estimateTripTime(cmd_source,rasa_message_list):
    dest = rasa_message_list["dest"]
    time = estimate_trip_time(destination=dest)
    hours = time[0:2]
    minutes = time[3:5]
    res = "the trip to {} will takes {} hours and {} minutes ".format(dest,hours,minutes)
    reply(cmd_source,res)

def morningtext(cmd_source,rasa_message_list):
    morning_dict = morning_text_form()
    res = "getting the morning text"
    reply(cmd_source,res)

def skipMorning(cmd_source,rasa_message_list):
    res = "skipping"
    reply(cmd_source,res)

def stopMorning(cmd_source,rasa_message_list):
    res = "stopping"
    reply(cmd_source,res)

def emergency(cmd_source,rasa_message_list):
    res = "calling emergency"
    reply(cmd_source,res)

def getWIFIpass(cmd_source,rasa_message_list):
    res = "getting password"
    reply(cmd_source,res)

def provideLocation(cmd_source,rasa_message_list):
    place_location = rasa_message_list["place_location"]
    res = send_maps_url_to_app(place_location)
    if res and not cmd_source :
        res = "okay .. check your mobile application , i sent the location to you"
        reply(cmd_source,res)
    elif res and cmd_source :
        res = "okay .. here is the location"
        reply(cmd_source,res)
    else :
        res = "i am sorry , i can't reach your mobile , please make sure that you have installed our application"
        reply(cmd_source,res)


def placeRecommendation(cmd_source,rasa_message_list):
    recommended_place = rasa_message_list["recommended_place"]
    res = "I will get a list of places for you"
    reply(cmd_source,res)
    list_of_places = places_recommendation_with_keyword(keyword=recommended_place, limit=5)
    if list_of_places == False :
        reply(cmd_source,"i am so sorry i can't get it")
    response = "here are some recomendations of places "
    for place in list_of_places :
        response = response + place["name"] + " in " + place["location_address"] + " and "
    reply(cmd_source,response[:-5])

def remindEvent(cmd_source,rasa_message_list):
    list_of_events = coming_events()
    if list_of_events==False :
        reply(cmd_source,"you don't have any events")
    response = "you have"
    for event in list_of_events :
        response = response + event + " and "
    reply(cmd_source,response[:-5])
def skip_remind_event(cmd_source,rasa_message_list):
    res = " skip_remind_event"
    reply(cmd_source,res)
def stop_remind_event(cmd_source,rasa_message_list):
    res = "  stop_remind_event "
    reply(cmd_source,res)

def get_hotel_sounds_playlist(cmd_source,rasa_message_list):
    res = "  get_hotel_sounds_playlist  "
    reply(cmd_source,res)

def skip_hotel_sounds(cmd_source,rasa_message_list):
    res = " skip_hotel_sounds "
    reply(cmd_source,res)

def stop_hotel_sounds(cmd_source,rasa_message_list):
    res = " stop_hotel_sounds "
    reply(cmd_source,res)

