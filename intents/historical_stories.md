## intent:tell_story
- what are the historical context of [Egypt](country)
- tell me a story about [France](country)
- let me know more about the history of [Italy](country)
- tell me something interesting about [Spain](country)