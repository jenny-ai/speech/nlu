## intent:call_uber
- can you provide me a drive to [the pyramids](destination)
- i need a ride to [the eiffel tower](destination) after [an hour](delay_time)
- reserve uber ride to [fish market restaurant](destination)
- my wife will arrive at [12 am](delay_time) please request for her a ride from [new airport](location) to [here](destination)
- i am worry to be locked on traffic at [12 pm](delay_time) after my visit to [the pyramids](location) can you request a ride to [hotel](destination)
- get me an uber ride to [Eiffel tower](destination)
