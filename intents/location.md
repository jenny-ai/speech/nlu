## intent:provide_location
- send me the location of [Eiffel tower](place)
- please i want to know the location of [Giza pyramides](place)
- tell me where is [carrefour](place)
- i want to know the location of [cairo tower](place)
- show me the location of [egyptian museum](place)
- where is [karnak temple](place)
- the location of [citadel of Qaitbay](place)
- give me the location of [the hanging church](place)
- tell me the location of [AlAzhar park](place)
- i want to know the location of [mosque of Muhammad Ali](place) on the map
- where is [Statue of Liberty](place) on the map

## intent:inform_place
- [cairo fesitival](place)
- the place is [sun city mall](place)
- the location of [Ainshams university](place)