## intent:translate
- translate [homogeneous](words) to [spanish](language)
- what is the translation of [can someone come to take my laundry](words) to [hindi](language)
- let me know the meaning of [naranja](words) in [english](language)
- what is the meaning of [good morning](words) in [turkish](language)