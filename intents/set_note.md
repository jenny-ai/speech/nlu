## intent:set_note
- please set a note
- i want to set a note
- set my note please 
- i need to set a note
- set a note
- set  note
- save this note 

## intent:inform_note
- [my sister has broken her mobile](note)
- [my doctor need an assignment next week](note)
- [i want to get my clothes from the dry clean](note)
- [I got the full mark in the project](note)
- [i have to buy the vegetables to my mother](note)
- [i paid the internet bill](note)
- [i have an important meeting](note)