 
## story_goodbye
* goodbye
 - utter_goodbye
 - action_restart
 
## story_thanks
* thanks
 - utter_thanks
 - action_restart
 
## story_affirm
* affirm
 - utter_happy
 - action_restart
 
## story_deny
* deny
 - utter_denyresponse
 - action_restart
 
## story_joke_01
* joke
 - action_joke
* affirm
 - action_joke
 - action_restart
 
## story_joke_02
* joke
 - action_joke
* deny
 - action_not_joke
 - action_restart

## Generated Story_joke_loop_03
* joke
    - action_joke
* affirm
    - action_joke
* affirm
    - action_joke
* affirm
    - action_joke
* affirm
    - action_joke
* deny
    - action_not_joke
    - action_restart


## story_ask_to_wake_up_01
* ask to wake up at a certain time
 - wake_form
 - form{"name": "wake_form"}
 - form{"name": null}
* affirm
 - action affirm wake
 - action_restart

## story_ask_to_wake_up_02
* ask to wake up at a certain time
 - wake_form
 - form{"name": "wake_form"}
 - form{"name": null}
* deny
 - action deny wake
 - action_restart

## Generated Story ask_to_wake_up_03_hapypath
* ask to wake up at a certain time{"time": "9 pm", "day": "today"}
    - slot{"day": "today"}
    - slot{"time": "9 pm"}
    - wake_form
    - form{"name": "wake_form"}
    - slot{"time": "9 pm"}
    - slot{"day": "today"}
    - slot{"time": "9 pm"}
    - slot{"day": "today"}
    - form{"name": null}
    - slot{"requested_slot": null}
* affirm
    - action affirm wake
    - slot{"time": null}
    - slot{"day": null}
    - action_restart

## story_recommendations_for_food_restaurants_01
* recommendations for food restaurants
 - food_form
 - form{"name": "food_form"}
 - form{"name": null}
 - action_restart
 
## story_play_music_01
* play music/ audio book
 - music_form
 - form{"name": "music_form"} 
 - form{"name": null}
 - action_restart
 
## story_TurningTVON_01_happypath
* TurnOnTV
 - action_turnTVon
 - action_restart
 
## story_turningTVOFF_01_happypath
* turnoffTV
 - action_turnTVoff
 - action_restart
 
## story_IncreaseTVvolume_01_happypath
* increaseVolume
 - action_increaseVol
 - action_restart
 
## story_DecreaseTVvolume_01_happypath
* decreaseVolume
 - action_decreaseVol
 - action_restart


## Generated StoryEstmatingTripTime_01_happypath
* estimate_trip_time{"source": "cairo", "destination": "alexandria"}
    - slot{"destination": "alexandria"}
    - slot{"source": "cairo"}
    - tripTime_form
    - form{"name": "tripTime_form"}
    - slot{"source": "cairo"}
    - slot{"destination": "alexandria"}
    - slot{"source": "cairo"}
    - slot{"destination": "alexandria"}
    - slot{"destination": null}
    - slot{"source": null}
    - form{"name": null}
    - slot{"requested_slot": null}
    - action_restart

## Generated Story 8889903771210993236
* estimate_trip_time{"source": "cairo", "destination": "alex"}
    - slot{"destination": "alex"}
    - slot{"source": "cairo"}
    - tripTime_form
    - form{"name": "tripTime_form"}
    - slot{"source": "cairo"}
    - slot{"destination": "alex"}
    - slot{"source": "cairo"}
    - slot{"destination": "alex"}
    - slot{"destination": null}
    - slot{"source": null}
    - form{"name": null}
    - slot{"requested_slot": null}
    - action_restart

## Generated Story 1032739621866524291
* estimate_trip_time{"destination": "giza pyramids"}
    - slot{"destination": "giza pyramids"}
    - tripTime_form
    - form{"name": "tripTime_form"}
    - slot{"destination": "giza pyramids"}
    - slot{"destination": "giza pyramids"}
    - slot{"destination": null}
    - slot{"source": null}
    - form{"name": null}
    - slot{"requested_slot": null}
    - action_restart

## Generated Story -1495241321911853077
* estimate_trip_time{"destination": "barcelona"}
    - slot{"destination": "barcelona"}
    - tripTime_form
    - form{"name": "tripTime_form"}
    - slot{"destination": "barcelona"}
    - slot{"destination": "barcelona"}
    - slot{"destination": null}
    - slot{"source": null}
    - form{"name": null}
    - slot{"requested_slot": null}
    - action_restart

## Generated Story -2567320375339292926
* estimate_trip_time{"destination": "the egyptian museum"}
    - slot{"destination": "the egyptian museum"}
    - tripTime_form
    - form{"name": "tripTime_form"}
    - slot{"destination": "the egyptian museum"}
    - slot{"destination": "the egyptian museum"}
    - slot{"destination": null}
    - slot{"source": null}
    - form{"name": null}
    - slot{"requested_slot": null}
    - action_restart

## story_morningtext_01_happypath
* morning_text
 - action_morning
 - action_restart
## story_morningtext_02_happypathskip
* morning_text
 - action_morning
* skip
 - action_skip_morning
 - action_restart

## story_morningtext_03_happypathskip
* morning_text
 - action_morning
* skip
 - action_skip_morning
* skip
 - action_skip_morning
 - action_restart
## story_morningtext_03_happypath
* morning_text
 - action_morning
* skip
 - action_skip_morning
* skip
 - action_skip_morning
* stop
 - action_stop_morning
 - action_restart

## Generated Story_morningtext_04_happypath
* morning_text
    - action_morning
* skip
    - action_skip_morning
* skip
    - action_skip_morning
* skip
    - action_skip_morning
* stop
    - action_stop_morning
    - action_restart


## story_morningtext_04_happypath
* morning_text
 - action_morning
* stop
 - action_stop_morning
 - action_restart

## story_emergency_01_happypath
* emergency
 - action_emergency
 - action_restart
## story_askwifipass_01_happypath
* AskWIFIpassword
 - action_wifipass
 - action_restart
## story_place_recommendation
* place_recommendation
 - action_recommendation
 - form{"name":"action_recommendation"}
 - form{"name":null}
 - action_restart
## story_provide_location
* provide_location
 - action_location
 - form{"name":"action_location"}
 - form{"name":null}
 - action_restart
 ## storyHM1
* hotel_music
- hotel_sounds
- action_restart
## storyHM2
* hotel_music
- hotel_sounds
* skip
- skip_hotel_sounds
* skip
- skip_hotel_sounds
* skip
- skip_hotel_sounds
- action_restart
## storyHM3
* hotel_music
- hotel_sounds
* skip
- skip_hotel_sounds
- action_restart

## storyHM4
* hotel_music
- hotel_sounds
* skip
- skip_hotel_sounds
* stop
- stop_hotel_sounds
- action_restart

## storyHM5
* hotel_music
- hotel_sounds
* skip
- skip_hotel_sounds
* hotel_music
- hotel_sounds
* skip
- skip_hotel_sounds
- action_restart

## storyHM6
* hotel_music
- hotel_sounds
* stop
- stop_hotel_sounds
- action_restart

## storyHM2
* hotel_music
- hotel_sounds
* skip
- skip_hotel_sounds
* skip
- skip_hotel_sounds
* skip
- skip_hotel_sounds
* stop
- stop_hotel_sounds
- action_restart

## storyER1
* EventReminder
- remind_event
- action_restart

## storyER2
* EventReminder
- remind_event
* skip
- skip_remind_event
* skip
- skip_remind_event
* skip
- skip_remind_event
- action_restart

## storyER3
* EventReminder
- remind_event
* skip
- skip_remind_event
- action_restart

## storyER4
* EventReminder
- remind_event
* skip
- skip_remind_event
* skip
- skip_remind_event
* stop
- stop_remind_event
- action_restart

## storyER5
* eventReminder
- remind_event
* skip
- skip_remind_event
* skip
- skip_remind_event
- action_restart

## storyER6
* EventReminder
- remind_event
* stop
- stop_remind_event
- action_restart

## storyER7
* EventReminder
- remind_event
* skip
- skip_remind_event
* skip
- skip_remind_event
* skip
- skip_remind_event
* stop
- stop_remind_event
- action_restart

## Generated Story 6177012532894331396
* joke
    - action_joke
* affirm
    - action_joke
* deny
    - action_not_joke
    - action_restart

## Generated Story -7720723531321153920
* recommendations for food restaurants
    - food_form
    - form{"name": "food_form"}
    - slot{"requested_slot": "food"}
* form: type of food{"food": "spanish"}
    - slot{"food": "spanish"}
    - form: food_form
    - slot{"food": "spanish"}
    - slot{"food": null}
    - form{"name": null}
    - slot{"requested_slot": null}
    - action_restart

## Generated Story 0

## Generated Story -3552050137501530792
* stop

## Generated Story 736047919982518764
* recommendations for food restaurants
    - food_form
    - form{"name": "food_form"}
    - slot{"requested_slot": "food"}
* type of food
    - action_default_fallback
    - rewind
    - recommend_restaurant_form

## Generated Story -3659980103390418174
* provide_location{"place_location": "arc de triomphe"}
    - slot{"place_location": "arc de triomphe"}
    - action_location
    - form{"name": "action_location"}
    - slot{"place_location": "arc de triomphe"}
    - slot{"place_location": "arc de triomphe"}
    - slot{"place_location": null}
    - form{"name": null}
    - slot{"requested_slot": null}
    - action_restart

## Generated Story 4650077811437483219
* provide_location
    - utter_ask_place_location
    - action_restart

## Generated Story 7048152774222062362
* location
    - action_restart

## Generated Story 6518109904616600904
* provide_location
    - action_location
    - form{"name": "action_location"}
    - slot{"requested_slot": "place_location"}
* form: location

## Generated Story 0

