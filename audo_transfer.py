import wavio
from jenny.communication import send_to_local_server
from gevent import sleep

a = wavio.read("demo4.wav")
send_to_local_server(data_id='V', data=[{
    "sampling_rate": a.rate,
    "sample_width": a.sampwidth,
    "audio_data": a.data.tolist()
    }])

sleep(30)
