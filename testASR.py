


from ASR import kaldi_decode_wav


kaldi_model = kaldi_decode_wav.loadKaldiModel()
command = kaldi_decode_wav.speechRecognizer(kaldi_model)
wavfile = kaldi_decode_wav.listen()

print(command)
