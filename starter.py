from TTS.flite import textToSpeech
from snowboy.hotword import detectHotword
from ASR import kaldi_decode_wav
from NLU.dict_nlu import askjenny 




if __name__ == "__main__" :


    #load models
    """[loading all required models]
    """
    kaldi_model = kaldi_decode_wav.loadKaldiModel()
    flag_new_registration = True #get it from backend
    #rasa_flag = False
    guestname= "Peter" #get it from backend
    while True :
        ## voice interface pipline
        """ hotkey -> ASR -> NLU -> TTS -> and so on >>>> response "done"
        """
        if detectHotword():
            if flag_new_registration == True :
                #calling welcome message function
                welcome = "Hey {} I'm jenny, your hotel assistant box.I can help you with....".format(guestname) # not implemented
                textToSpeech(welcome)
                
                flag_new_registration = False #set it to backend
            #after finishing each story get out rasa loop and wait another hotkey detection
            rasa_flag=True
            while (rasa_flag):
                rasa_flag=False
                command = kaldi_decode_wav.speechRecognizer(kaldi_model)
                "or get command from APP"
                #not implemented

                ## sending the command string to rasa NLU
                #rasa_flag , response = sendcommand(command)
                rasa_flag = askjenny(False,command)
                #print(response)
                #textToSpeech(response)




        #text = "I am sorry , I can't understand you"

        #textToSpeech(text)
