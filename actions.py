
from typing import Dict, Text, Any, List, Union, Optional
from rasa_core_sdk import Action
from rasa_core_sdk import ActionExecutionRejection
from rasa_core_sdk import Tracker
from rasa_core_sdk.events import SlotSet
from rasa_core_sdk.executor import CollectingDispatcher
from rasa_core_sdk.forms import FormAction, REQUESTED_SLOT




class ActionQuery(Action):
    def name(self):
        # define the name of the action which can then be included in training stories
        return "action_queries"

    def run(self, dispatcher, tracker, domain):
        # what your action should do
        query = tracker.get_slot("query")
        # in case of place entity
        #if(query==None):
        #    query = tracker.get_slot("place")
        response = get_query(query)
        dispatcher.utter_message(response)  # send the message back to the user
        return [SlotSet("query", None)]


class ActionNewMail(Action):
    def name(self):
        # define the name of the action which can then be included in training stories
        return "action_getMails"

    def run(self, dispatcher, tracker, domain):
        response = get_mails()
        dispatcher.utter_message(response)  # send the message back to the user
        return []
        
"""
class ActionNote(Action):
    def name(self):
        return "action_setnote"

    def run(self, dispatcher, tracker, domain):

        note = tracker.get_slot("note")

        response = set_note(note)
        dispatcher.utter_message(response)  # send the message back to the user
        return [SlotSet("note", None)]
"""
class NoteForm(FormAction):
    def name(self):
        # type: () -> Text
        return "note_form"

    @staticmethod
    def required_slots(tracker: Tracker) -> List[Text]:
        """A list of required slots that the form has to fill"""
        return ["note"]

    def submit(self,
                dispatcher: CollectingDispatcher,
                tracker: Tracker,
                domain: Dict[Text, Any]) -> List[Dict]:
        """Define what the form has to do
            after all required slots are filled"""
        # utter submit template
        note = tracker.get_slot('note')
        response = set_note(note)
        dispatcher.utter_message(response)  
        return [SlotSet("note", None)]

class LocationForm(FormAction):
    """Example of a custom form action"""

    def name(self):
        # type: () -> Text
        """Unique identifier of the form"""

        return "location_form"

    @staticmethod
    def required_slots(tracker: Tracker) -> List[Text]:
        """A list of required slots that the form has to fill"""

        return ["place"]

    
    def slot_mappings(self):
        # type: () -> Dict[Text: Union[Dict, List[Dict]]]
        """A dictionary to map required slots to
            - an extracted entity
            - intent: value pairs
            - a whole message
            or a list of them, where a first match will be picked"""

        return {"place": [self.from_entity(entity="place",
                                            intent=["inform_place","provide_location"]),
                            self.from_entity(entity="query",intent="queries")]}
    
    def submit(self,
                dispatcher: CollectingDispatcher,
                tracker: Tracker,
                domain: Dict[Text, Any]) -> List[Dict]:
        """Define what the form has to do
            after all required slots are filled"""

        # utter submit template
        place = tracker.get_slot('place')
        response = get_location(place)
        dispatcher.utter_message(response)  
        return [SlotSet("place", None)]
'''
class ActionProvideLocation(Action):
    def name(self):
        # type: () -> Text
        return "action_provide_location"

    def run(self, dispatcher, tracker, domain):
        # type: (CollectingDispatcher, Tracker, Dict[Text, Any]) -> List[Dict[Text, Any]]

        place = tracker.get_slot('place')
        response = get_location(place)
        dispatcher.utter_message(response)  
        return []
'''
def get_query(query):
    string = "searching for {}".format(query)
    return string

def get_location(place):
    string = " get '{}' location in map".format(place)
    return string

def get_mails():
    string = "getting new mails in your inbox"
    return string

def set_note(note):
    string = "set [{}] as a note".format(note)
    return string