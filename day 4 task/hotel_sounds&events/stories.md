## storyHM1
* hotel_music
- hotel_sounds

## storyHM2
* hotel_music
- hotel_sounds
* skip
- skip_hotel_sounds
* skip
- skip_hotel_sounds
* skip
- skip_hotel_sounds

## storyHM3
* hotel_music
- hotel_sounds
* skip
- skip_hotel_sounds


## storyHM4
* hotel_music
- hotel_sounds
* skip
- skip_hotel_sounds
* stop
- stop_hotel_sounds


## storyHM5
* hotel_music
- hotel_sounds
* skip
- skip_hotel_sounds
* hotel_music
- hotel_sounds
* skip
- skip_hotel_sounds


## storyHM6
* hotel_music
- hotel_sounds
* stop
- stop_hotel_sounds

## storyHM2
* hotel_music
- hotel_sounds
* skip
- skip_hotel_sounds
* skip
- skip_hotel_sounds
* skip
- skip_hotel_sounds
* stop
- stop_hotel_sounds

## storyER1
* eventReminder
- remind_event

## storyER2
* eventReminder
- remind_event
* skip
- skip_remind_event
* skip
- skip_remind_event
* skip
- skip_remind_event

## storyER3
* eventReminder
- remind_event
* skip
- skip_remind_event


## storyER4
* eventReminder
- remind_event
* skip
- skip_remind_event
* skip
- skip_remind_event
* stop
- stop_remind_event


## storyER5
* eventReminder
- remind_event
* skip
- skip_remind_event
* skip
- skip_remind_event


## storyER6
* eventReminder
- remind_event
* stop
- stop_remind_event

## storyER7
* eventReminder
- remind_event
* skip
- skip_remind_event
* skip
- skip_remind_event
* skip
- skip_remind_event
* stop
- stop_remind_event
