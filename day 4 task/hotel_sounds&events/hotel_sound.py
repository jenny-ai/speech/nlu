class ActionHotelSounds(Action):
    def name(self):
        # define the name of the action which can then be included in training stories
        return "hotel_sounds"

    def run(self, dispatcher, tracker, domain):
        # what your action should do
        response = get_hotel_sounds_playlist()
        dispatcher.utter_message(response)  # send the message back to the user
        return []

class ActionSkipHotelSounds(Action):
    def name(self):
        # define the name of the action which can then be included in training stories
        return "skip_hotel_sounds"

    def run(self, dispatcher, tracker, domain):
        # what your action should do
        response = skip_hotel_sounds()
        dispatcher.utter_message(response)  # send the message back to the user
        return []

class ActionStopHotelSounds(Action):
    def name(self):
        # define the name of the action which can then be included in training stories
        return "stop_hotel_sounds"

    def run(self, dispatcher, tracker, domain):
        # what your action should do
        response = stop_hotel_sounds()
        dispatcher.utter_message(response)  # send the message back to the user
        return []
