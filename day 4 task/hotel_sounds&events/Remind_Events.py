class ActionRemindEvent(Action):
    def name(self):
        # define the name of the action which can then be included in training stories
        return "remind_event"

    def run(self, dispatcher, tracker, domain):
        # what your action should do
        response = remindEvent()
        dispatcher.utter_message(response)  # send the message back to the user
        return []

class ActionSkipRemindEvent(Action):
    def name(self):
        # define the name of the action which can then be included in training stories
        return "skip_remind_event"

    def run(self, dispatcher, tracker, domain):
        # what your action should do
        response = skip_remind_event()
        dispatcher.utter_message(response)  # send the message back to the user
        return []

class ActionStopRemindEvent(Action):
    def name(self):
        # define the name of the action which can then be included in training stories
        return "stop_remind_event"

    def run(self, dispatcher, tracker, domain):
        # what your action should do
        response = stop_remind_event()
        dispatcher.utter_message(response)  # send the message back to the user
        return []
