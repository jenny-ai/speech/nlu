class locationForm(FormAction):
   """Example of a custom form action"""

   def name(self):
       # type: () -> Text
       """Unique identifier of the form"""

       return "location_form"

   @staticmethod
   def required_slots(tracker):
       # type: () -> List[Text]
       """A list of required slots that the form has to fill"""

       return ["place_location"]

   def submit(self, dispatcher, tracker, domain):
       # what your action should do
        dest = tracker.get_slot("place_location")
        response = provideLocation(place_location)
        dispatcher.utter_message(response)  # send the message back to the user
        return [SlotSet("place_location", None)]

def provideLocation(place_location):
    res = "get the place location {}".format(place_location)
    return res
	
	
class recommendationForm(FormAction):
   """Example of a custom form action"""

   def name(self):
       # type: () -> Text
       """Unique identifier of the form"""

       return "recommendation_form"

   @staticmethod
   def required_slots(tracker):
       # type: () -> List[Text]
       """A list of required slots that the form has to fill"""

       return ["recommended_place"]

   def submit(self, dispatcher, tracker, domain):
       # what your action should do
        dest = tracker.get_slot("recommended_place")
        response = placeRecommendation(recommended_place)
        dispatcher.utter_message(response)  # send the message back to the user
        return [SlotSet("recommended_place", None)]

def placeRecommendation(recommended_place):
    res = "get the recommended place {}".format(recommended_place)
    return res