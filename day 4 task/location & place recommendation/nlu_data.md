## intent:provide_location
- where is [the nearest bank](place_location_location)
- how do you get to [alberta](place_location)
- what is the location of [sydney opera house](place_location)
- how can i go to [the egyptian museum ](place_location)
- send me the location of [the river nile](place_location)
- please i want to know the location of [Giza pyramids](place_location)
- tell me where is [carrefour](place_location)
- i want to know the location of [cairo tower](place_location)
- show me the location of [egyptian museum](place_location)
- the location of [citadel of Qaitbay](place_location)
- give me the location of [the hanging church](place_location)
- tell me the location of [AlAzhar park](place_location)
- i want to know the location of [mosque of Muhammad Ali](place_location) on the map
- where is [Statue of Liberty](place_location) on the map
- show me the path to [arc de triomphe](place_location)

## intent:inform_place_location
- [cairo fesitival](place_location)
- the place_location is [sun city mall](place_location)
- the location of [Ainshams university](place_location)
- the path to [the alps](place_location)
- the location is [the egyptian museum](place_location)

## intent:place_recommendation
- would you recommend me [a museum](recommended_place) to visit
- what is the best [cinema](recommended_place) around here
- recomment me [a gym](recommended_place_location) to go
- tell me what is the best [club](recommended_place) near the hotel
- give me a recommendation about [shopping mall](recommended_place)
- which [mall](recommended_place) has the highest ratings
- can you tell me where i can find [a spa](recommended_place)